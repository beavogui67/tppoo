#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 20:53:17 2021

@author: angelina
"""

# Node class
class Node:
    """
    Class to represent each node of the linked list
    """
    # pylint: disable=too-few-public-methods

    def __init__(self, param_data):
        """
        Method to initialize the node object

        The value of the data will be defined by the value passed through the constructor,
        and the pointer(link) will be initially set to null.
        """

        #Assign data
        self.data = param_data

        # Initialize link as null
        self.link = None


    def __str__(self):
        """
        Method to display the nodes and its links.

        Returns
        -------
        str
            node list and its links
        """

        liste = []
        node = self
        while node.link is not None:
            liste.append(node.data)
            node = node.link
        liste.append(node.data)
        return str(liste)
