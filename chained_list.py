#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 20:53:17 2021

@author: angelina
"""

from __future__ import absolute_import
from node import Node

# Linked list class
class Chainedlist:
    """
    Chained list Object
    Contains the methods for inserting and/or deleting an item in the chained list.

    Parameters
    ----------
    nodes : list
        list that we want to transfert in a chained list of Node object.
    """


    def __init__(self, nodes):
        """
        Method to initialize the Linked list.

        We initialize each element of the list entered by the user as a node
        and we add a link to it, to obtain a chained list.
        """

        # start_node that will point to the starting or first node of the list.
        self.start_node = Node(nodes[0])
        curr_node = self.start_node

        # Browse the list from the first to the last element,
        # creating a node and a link for each element.
        for item in nodes[1:]:
            curr_node.link = Node(item)
            curr_node = curr_node.link


    def __str__(self):
        """
        Method to display the chained list.

        Returns
        -------
        str
            Displays the chained list.
        """
        return str(self.start_node)


    def insert_node_after(self, node, new_node):
        """
        Method that allows to insert a new node after the node with the value == node

        Parameters
        ----------
        node : searched data, the node after which you want to insert the new node.
        new_node : node to insert, contains the value for the new node.
        """
        item = self.start_node

        # Browse the chained list until item becomes None
        while item is not None:

            # We check if the value stored in the current node is equal to the
            # value passed by the node parameter.
            # If the comparison returns true, we break the loop.
            if item.data == node:
                break
            item = item.link
        if item is None:
            print("The mentioned node is absent")
        else:
            # The variable new_data create a new node containing the value
            # entered by the user (new_node).
            new_data = Node(new_node)

            # The pointer(link) of the new_data is set to link stored by item
            # and the link of item is set to new_data.
            new_data.link = item.link
            item.link = new_data


    def delete_node(self, node):
        """
        Method that allows to delete all node(s) value == node

        Parameters
        ----------
        node : searched data to delete
        """

        # Check if the list is empty
        if self.start_node is None:
            print("The list has no element to delete")
            return

        # We check if the item to be deleted is at the beginning of the chained list.
        # If True , it is deleted by defining the second node as the first node.
        if self.start_node.data == node:
            self.start_node = self.start_node.link
            return

        item = self.start_node
        while item.link is not None:
            if item.link.data == node:
                break
            item = item.link

        if item.link is None:
            print("The mentioned node is absent")
        else:
            # We set the link of the previous node to the node which exists
            # after the node which is being deleted.
            item.link = item.link.link
