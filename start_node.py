#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 20:53:17 2021

@author: angelina
"""

from __future__ import absolute_import
from node import Node
from chained_list import Chainedlist

def test_print_node():
    """
    The function creates three nodes that are connected by a link.

        first        second              third
         |                |                  |
         |                |                  |
    +----+------+     +----+------+     +----+------+
    | 1  |  o-------->| 5  |  o-------->| 10 | null |
    +----+------+     +----+------+     +----+------+
    """
    first = Node(1)
    second = Node(5)
    third = Node(10)
    first.ref = second
    second.ref = third
    #print(first)


def insert_node_chainliste():
    """
    This function calls the Chainedlist class,
    and uses the insert_node_after method, to insert a node anywhere in the linked list.
    """

    chained_list = Chainedlist([1,5,6,12,34])
    print("Chained list before insertion :", chained_list)
    chained_list.insert_node_after(5,188)
    print("Chained list after insertion :", chained_list)


def delete_node_chainliste():
    """
    This function calls the ChainedList class,
    and uses the delete_node method, to delete a node anywhere in the list.
    """

    chained_list = Chainedlist([1,5,6,12,34])
    print("chained list before deleting the node :", chained_list)
    chained_list.delete_node(5)
    print("chained list after deleting the node :", chained_list)

# Code execution starts here
if __name__ == "__main__":
    test_print_node()
    insert_node_chainliste()
    delete_node_chainliste()
